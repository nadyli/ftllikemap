﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScreenControls : MonoBehaviour {
    RaycastHit hit;
    Ray ray;
    public GameObject target = null;

    void Update() {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit)) {
            if (hit.collider.gameObject.transform.parent.GetComponent<Node>()) {
                target = hit.collider.gameObject.transform.parent.gameObject;
            }
        } else {
            if(target) {
                target.GetComponent<Node>().ToggleOff();
                target = null;
            }
        }
        if(target) {            
            target.GetComponent<Node>().ToggleOn();
        }
    }
}
