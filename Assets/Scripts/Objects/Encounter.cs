﻿public class Encounter {
    int wood;
    int stone;
    string text;

    public Encounter() {
        wood = 0;
        stone = 0;
        text = "";
    }
    public Encounter(int w, int s, string t) {
        wood = w;
        stone = s;
        text = t;
    }
}
