﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
	float size = 0.5f;
	public int row;
	public int myPosition;
    int eventID;
    string eventText;
    bool active = false;
	public List<GameObject> connectedNodes = new List<GameObject>();
    Material myMaterial;

    private void Awake() {
        CreateObject();        
    }

    private void CreateObject() {
        GameObject shape = GameObject.CreatePrimitive(PrimitiveType.Cube);
		shape.transform.localScale = new Vector3(size, size, size);
        myMaterial = shape.GetComponent<Renderer>().material;
        shape.transform.SetParent(transform);
    }

    private void Update() {
        if(active) {
           myMaterial.color = Color.green;
        } else {
           myMaterial.color = Color.grey;
        }
    }

	public void AddConnectedNode(GameObject go) {
		connectedNodes.Add(go);
	}

    public void ToggleOn() {
        active = true;
    }

    public void ToggleOff() {
        active = false;
    }
}
