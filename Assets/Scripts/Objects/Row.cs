﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Row {
	public List<GameObject> nodes = new List<GameObject>();

	public void AddNode(GameObject go) {
		nodes.Add(go);
	}

	public void RemoveNode(GameObject go) {
		nodes.Remove(go);
	}
}
