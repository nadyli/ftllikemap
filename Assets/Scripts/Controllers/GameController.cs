﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour {
    private static GameController _instance;
    public static GameController Instance {
        get { return _instance; }
    }

    /// <summary>
    /// 0 = mainmenu
    /// 1 = map
    /// 2 = event
    /// </summary>
    public int CurrentScreen = 0;

    private void Awake() {
        if(_instance != null && _instance != this) {
            Destroy(gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void SwitchScene(string sceneName) {
        switch (sceneName) {
            case "MapScreen":
                Debug.Log("Switching to map scene.");
                SceneManager.LoadScene("MapScreen");
                break;
            default:
                Debug.Log("Unknown scene.");
                break;
        }
    }
}
