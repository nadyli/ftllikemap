﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScreenController : MonoBehaviour {
	int nodeNumber;
	int rowNumber;
	int screenWidth;
	int screenHeight;

	public int NodeNetworkRowAmount = 5;

	public Camera mainCamera;

	public GameObject node;
	public GameObject nodes;
	public GameObject first;
	public GameObject last;

	public List<Row> rows = new List<Row>();

	private void Awake() {
		mainCamera = Camera.main;

		screenWidth = mainCamera.pixelWidth;
		screenHeight = mainCamera.pixelHeight;

		GenerateNetwork();
	}

	public void GenerateNetwork() {
		ClearNodes();
		CreateNetwork();

		while(!FindRoute()) {
			ClearNodes();
			CreateNetwork();
		}

		FindConnectedNodes();
		DeleteObsolete();
	}

	private void ClearNodes() {
		rows.Clear();
		first = null;
		last = null;

		Node[] nodes = FindObjectsOfType(typeof(Node)) as Node[];

		foreach(Node no in nodes) {
			Destroy(no.gameObject);
		}
	}

	private void CreateNetwork() {
		nodeNumber = 0;
		rowNumber = 0;

		CreateRow(true);

		for(int i = 0; i < NodeNetworkRowAmount; i++) {
			CreateRow(false);
		}

		CreateRow(true);
	}

	void CreateRow(bool endPoint) {
		// TODO: Max amount of rows that have 3 nodes?
		if(endPoint) {
			Row row = new Row();
			row.AddNode(CreateNode(2));
			rows.Add(row);
			rowNumber++;
		} else {
			Row row = new Row();
			int randomNodeAmount = Random.Range(1, 4);
			int randomPosition = Random.Range(1, 5);

			if(randomNodeAmount == 1) {
				row.AddNode(CreateNode(Random.Range(1, 6)));
			}
			if(randomNodeAmount == 2) {
				if(randomPosition == 1) {
					row.AddNode(CreateNode(1));
					row.AddNode(CreateNode(2));
				}
				if(randomPosition == 2) {
					row.AddNode(CreateNode(2));
					row.AddNode(CreateNode(3));
				}
				if(randomPosition == 3) {
					row.AddNode(CreateNode(1));
					row.AddNode(CreateNode(3));
				}
				if(randomPosition == 4) {
					row.AddNode(CreateNode(4));
					row.AddNode(CreateNode(5));
				}
			}
			if(randomNodeAmount == 3) {
				row.AddNode(CreateNode(1));
				row.AddNode(CreateNode(2));
				row.AddNode(CreateNode(3));
			}

			rows.Add(row);
			rowNumber++;
		}
	}

	GameObject CreateNode(int position) {
		GameObject go = Instantiate(node, Vector3.zero, Quaternion.identity);
		go.name = "node" + nodeNumber + " row: " + rowNumber;
		go.transform.position = GetPosition(position);
		go.GetComponent<Node>().row = rowNumber;
		go.GetComponent<Node>().myPosition = position;
		go.transform.SetParent(nodes.transform);

		if(nodeNumber++ == 0) {
			first = go;
		}
		last = go;

		return go;
	}

	Vector3 GetPosition(int position) {
		int firstPosition = 25;
		int secondPosition = 50;
		int thirdPosition = 75;
		int fourthPosition = 37;
		int fifthPosition = 63;

		float dividedPercentageValue = 100 / (NodeNetworkRowAmount + 3);

		Vector3 tempPosition;
		float rowPosition = dividedPercentageValue + dividedPercentageValue * rowNumber;
		switch(position) {
			case 1:
			tempPosition = CalculatePosition((int)rowPosition, firstPosition);
			break;
			case 2:
			tempPosition = CalculatePosition((int)rowPosition, secondPosition);
			break;
			case 3:
			tempPosition = CalculatePosition((int)rowPosition, thirdPosition);
			break;
			case 4:
			tempPosition = CalculatePosition((int)rowPosition, fourthPosition);
			break;
			case 5:
			tempPosition = CalculatePosition((int)rowPosition, fifthPosition);
			break;
			default:
			tempPosition = CalculatePosition(0, 0);
			break;
		}
		return tempPosition;
	}

	private bool CheckIfCanConnect(GameObject node, GameObject node2) {
		switch(node.GetComponent<Node>().myPosition) {
			case 1:
			if(node2.GetComponent<Node>().myPosition == 1)
				return true;
			if(node2.GetComponent<Node>().myPosition == 2)
				return true;
			if(node2.GetComponent<Node>().myPosition == 4)
				return true;
			return false;
			case 2:
			if(node2.GetComponent<Node>().myPosition == 1)
				return true;
			if(node2.GetComponent<Node>().myPosition == 2)
				return true;
			if(node2.GetComponent<Node>().myPosition == 3)
				return true;
			if(node2.GetComponent<Node>().myPosition == 4)
				return true;
			if(node2.GetComponent<Node>().myPosition == 5)
				return true;
			return false;
			case 3:
			if(node2.GetComponent<Node>().myPosition == 2)
				return true;
			if(node2.GetComponent<Node>().myPosition == 3)
				return true;
			if(node2.GetComponent<Node>().myPosition == 5)
				return true;
			return false;
			case 4:
			if(node2.GetComponent<Node>().myPosition == 1)
				return true;
			if(node2.GetComponent<Node>().myPosition == 2)
				return true;
			if(node2.GetComponent<Node>().myPosition == 4)
				return true;
			if(node2.GetComponent<Node>().myPosition == 5)
				return true;
			return false;
			case 5:
			if(node2.GetComponent<Node>().myPosition == 2)
				return true;
			if(node2.GetComponent<Node>().myPosition == 3)
				return true;
			if(node2.GetComponent<Node>().myPosition == 4)
				return true;
			if(node2.GetComponent<Node>().myPosition == 5)
				return true;
			return false;
			default:
			return false;
		}
	}

	private Vector3 CalculatePosition(int xPercent, int yPercent) {
		int tempX = (int)CalculatePercentageValue(xPercent, screenWidth);
		int tempY = (int)CalculatePercentageValue(yPercent, screenHeight);

		return CalculateScreenToWorld(tempX, tempY);
	}

	private float CalculatePercentageValue(float per, float for1) {
		float percent = 1 * (per / 100);
		float returnValue = for1 * percent;
		return returnValue;
	}

	private Vector3 CalculateScreenToWorld(int x, int y) {
		return mainCamera.ScreenToWorldPoint(new Vector3(x, y, 1));
	}

	private bool FindRoute() {
		bool routeFound = true;
		for(int i = 0; i < rows.Count - 1; i++) {
			if(!PathToNextRow(i))
				routeFound = false;
		}
		return routeFound;
	}

	bool PathToNextRow(int rN) {
		bool pathFound = false;
		for(int i = 0; i < rows[rN].nodes.Count; i++) {
			for(int y = 0; y < rows[rN + 1].nodes.Count; y++) {
				if(CheckIfCanConnect(rows[rN].nodes[i], rows[rN + 1].nodes[y]))
					pathFound = true;
			}
		}
		return pathFound;
	}

	void FindConnectedNodes() {
		// Menemme kaikki rowit läpi
		for(int i = 0; i < rows.Count; i++) {
			// Kuinka monta nodea meillä on tällä rivillä
			for(int y = 0; y < rows[i].nodes.Count; y++) {
				// Onko meillä seuraava rivi				
				if(i + 1 < rows.Count) {
					// Kuinka monta nodea on seuraavalla rivillä
					for(int z = 0; z < rows[i + 1].nodes.Count; z++) {
						// Tarkistetaan eteenpäin
						if(CheckIfCanConnect(rows[i].nodes[y], rows[i + 1].nodes[z]))
							rows[i].nodes[y].GetComponent<Node>().AddConnectedNode(rows[i + 1].nodes[z]);
					}
				}
				// Onko meillä takana rivi
				if(i - 1 >= 0) {
					// Kuinka monta nodea on takana olevalla rivillä
					for(int z = 0; z < rows[i - 1].nodes.Count; z++) {
						// Tarkistetaan taaksepäin
						if(CheckIfCanConnect(rows[i].nodes[y], rows[i - 1].nodes[z]))
							rows[i].nodes[y].GetComponent<Node>().AddConnectedNode(rows[i - 1].nodes[z]);
					}
				}
			}
			// Tarkistetaan oma rivi
			if(rows[i].nodes.Count == 2) {
				if(CheckIfCanConnect(rows[i].nodes[0], rows[i].nodes[1])) {
					rows[i].nodes[0].GetComponent<Node>().AddConnectedNode(rows[i].nodes[1]);
					rows[i].nodes[1].GetComponent<Node>().AddConnectedNode(rows[i].nodes[0]);
				}
			}
			if(rows[i].nodes.Count == 3) {
				rows[i].nodes[0].GetComponent<Node>().AddConnectedNode(rows[i].nodes[1]);
				rows[i].nodes[1].GetComponent<Node>().AddConnectedNode(rows[i].nodes[2]);

				rows[i].nodes[1].GetComponent<Node>().AddConnectedNode(rows[i].nodes[0]);
				rows[i].nodes[2].GetComponent<Node>().AddConnectedNode(rows[i].nodes[1]);
			}
		}
	}

	void DeleteObsolete() {
		Node[] nodes = FindObjectsOfType(typeof(Node)) as Node[];

		foreach(Node no in nodes) {
			if(no.connectedNodes.Count == 0) {
				foreach(Row row in rows) {
					if(row.nodes.Contains(no.gameObject)) {
						Debug.Log("Deleted obsolete node.");
						row.nodes.Remove(no.gameObject);
						Destroy(no.gameObject);
					}
				}
			}			
		}
	}
}
