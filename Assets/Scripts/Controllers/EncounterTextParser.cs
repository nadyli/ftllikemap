﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class EncounterTextParser : MonoBehaviour {
    string encountersUsed = "";
    Encounter GetEncounter(string encounterID) {
        string[] encounterResources = ReadFile();
        if(encounterResources == null) {
            Debug.Log("Couldn't load event resources.");
            return null;
        }
        string[] encounterData = FindEvent(encounterResources, encounterID);
        if(encounterData == null) {
            Debug.Log("Couldn't load event data for: " + encounterID);
            return null;            
        }
        if(encountersUsed == "") {
            encountersUsed += encounterID;
        } else {
            encountersUsed += ", " + encounterID;
        }

        Encounter encounter = ParseEvent(encounterData);
        return encounter;
    }

    string[] ReadFile() {
        string path = "Assets/Text/encounterData.txt";

        StreamReader reader = new StreamReader(path);
        string[] lines = File.ReadAllLines(path);

        reader.Close();

        return lines;
    }

    string[] FindEvent(string[] lines, string encounterID) {
        int start, end;

        start = Array.FindIndex(lines, element => element.Contains(encounterID));
        end = Array.FindIndex(lines, start, element => element.Contains("---"));

        string[] encounterArray = new string[end-start];

        if (start == -1 || end == -1) {
            return null;
        }

        Array.Copy(lines, start, encounterArray, 0, end - start);    
    
        return encounterArray;
    }
    Encounter ParseEvent(string[] lines) {
        int wood, stone;
        wood = stone = 0;
        string text = "";

        foreach(string s in lines) {
            if(s.Contains("text"))
               text = GetTextFromString(s);
            if(s.Contains("wood"))
                wood = GetResourceFromString(s);
            if(s.Contains("stone"))
                stone = GetResourceFromString(s);
        }

        Encounter encounter = new Encounter(wood, stone, text);

        return encounter;
    }

    int GetResourceFromString(string s) {
        int start = s.IndexOf('"');
        start++;
        int end = s.IndexOf('"', start);

        bool success = Int32.TryParse(s.Substring(start, end - start), out int resourceVal);

        if (success) {
            return resourceVal;
        }

        return 0;
    }

    string GetTextFromString(string s) {
        int start = s.IndexOf('"');
        start++;
        int end = s.IndexOf('"', start);        

        string resourceVal = s.Substring(start, end - start);
        return resourceVal;
    }
}
